var XNAT = getObject(XNAT || {});

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }
}(function(){

    console.log('xnat/plugin/mriqc_poldrack/displayReport.js');

    var reportList, displayContainer, sessionId,sessionLabel,
    	rootUrl = XNAT.url.rootUrl,
        restUrl = XNAT.url.restUrl,
        csrfUrl = XNAT.url.csrfUrl;

    var displayReportTable;

    XNAT.plugin =
        getObject(XNAT.plugin || {});

    XNAT.plugin.mriqc_poldrack =
        getObject(XNAT.plugin.mriqc_poldrack || {});

    XNAT.plugin.mriqc_poldrack.reportList = reportList = {};


   XNAT.plugin.mriqc_poldrack.displayReportTable = displayReportTable =
        getObject(XNAT.plugin.mriqc_poldrack.displayReportTable || {});

    function getMRIQCResourceUrl(session_id,appended){
        appended = (appended) ? '?'+appended : '';
        return restUrl('/data/experiments/' + session_id + '/resources/MRIQC/files/' + appended);
    }

    function errorHandler(e, title, closeAll){
        console.log(e);
        title = (title) ? 'Error Found: '+ title : 'Error';
        closeAll = (closeAll === undefined) ? true : closeAll;
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': '+ e.statusText+'</strong></p><p>' + e.responseText + '</p>' : e;
        XNAT.dialog.open({
            width: 450,
            title: title,
            content: errormsg,
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: true,
                    action: function(){
                        if (closeAll) {
                            xmodal.closeAll();

                        }
                    }
                }
            ]
        });
    }

function viewReportFile(e, onclose){
	e.preventDefault();
	var uri = $(this).data('URI');
	var name = $(this).data('Name');
	if (uri.endsWith(".csv")) {
           XNAT.ui.dialog.iframe(uri,'Session: '+ sessionLabel+ ' File: ' + name,580,600);
	}else {
           XNAT.ui.dialog.iframe(uri,'Session: '+ sessionLabel+ ' File: ' + name,580,600);
	}
}

function getFiles(session_id,callback){
	callback = isFunction(callback) ? callback : function(){};
	var URL = getMRIQCResourceUrl(session_id);
	return XNAT.xhr.getJSON(URL)
		.success(function(data){
			var fileResults = data.ResultSet.Result;
		   if (fileResults){
			   fileResults.forEach(function(fileEntry){
				  reportList[fileEntry.Name] = fileEntry;
			  });
		  }
		  callback.apply(this, arguments);
	   });
}

displayReportTable.showReportTable = function(){
       var $manager = displayContainer;

	   setTimeout(function(){
			_reportTable = XNAT.spawner.spawn({
				reportTable: spawnReportTable()
			});
			_reportTable.done(function(){
				this.render($manager, 20);
			});
		}, 10);
}

function spawnReportTable() {

        return {
            kind: 'table.dataTable',
            name: 'mriQCReport',
            id: 'mriQCReport',
            data: reportList,
            table: {
                classes: 'highlight',
                on: [
                    ['click', 'a.view-file', viewReportFile]
                ]
            },
            items: {
                file: {
                    label: 'Report Files',
                    apply: function(){
                        var label = this.Name;
						 return spawn('a.view-file', {
								href: '#!',
								title: 'View MRIQC Report HTML',
								data: {'Name':this.Name,'URI': this.URI },
								style: { wordWrap: 'break-word' },
								html: this.Name
						});
                    }
                }
            }
        }
    }





displayReportTable.init = displayReportTable.refresh = function(session_id, session_label,container) {
		sessionId = session_id;
		sessionLabel = session_label;
		displayContainer = container;
        var $manager = $$(container || '#mriqc_poldrack_table');
        $manager.parents('div[name="custom-post-scans"]').removeClass('hidden');
        getFiles(session_id);
}

}));



